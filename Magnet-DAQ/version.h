#ifndef VERSION_H
#define VERSION_H

// DO NOT use the ".y" in x.xx.y as it messes with MAO app version detect!
#define VER_FILEVERSION             1,1,8,0
#define VER_FILEVERSION_STR         "1.1.8.0\0"

#define VER_PRODUCTVERSION          1,1,8,0
#define VER_PRODUCTVERSION_STR      "1.18\0"

#define VER_COMPANYNAME_STR         "American Magnetics, Inc."
#define VER_FILEDESCRIPTION_STR     "Magnet-DAQ"
#define VER_INTERNALNAME_STR        "Magnet-DAQ"
#define VER_LEGALCOPYRIGHT_STR      "Copyright � 2025 American Magnetics, Inc."
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "Magnet-DAQ.exe"
#define VER_PRODUCTNAME_STR         "Magnet-DAQ"

#define VER_COMPANYDOMAIN_STR       "americanmagnetics.com"

#endif // VERSION_H
