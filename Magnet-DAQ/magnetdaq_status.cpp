#include "stdafx.h"
#include "magnetdaq.h"

//---------------------------------------------------------------------------
// Contains methods related to the Status tab.
// Broken out from magnetdaq.cpp for ease of editing.
//---------------------------------------------------------------------------

// local arrays for GUI
KLed* statusBit[8];
KLed* standardEventBit[8];
KLed* standardOperCondBit[16];
KLed* standardOperEventBit[16];
QCheckBox* statusBitEnable[8];
QCheckBox* stdEventEnable[8];
QCheckBox* stdOperEnable[16];
std::atomic<bool> isSyncing;

//---------------------------------------------------------------------------
void magnetdaq::restoreStatusSettings(QSettings *settings)
{
	isSyncing.store(false);

	// local GUI arrays
	statusBit[0] = ui.statusBit0;
	statusBit[1] = ui.statusBit1;
	statusBit[2] = ui.statusBit2;
	statusBit[3] = ui.statusBit3;
	statusBit[4] = ui.statusBit4;
	statusBit[5] = ui.statusBit5;
	statusBit[6] = ui.statusBit6;
	statusBit[7] = ui.statusBit7;

	statusBitEnable[0] = ui.statusBit0Enable;
	statusBitEnable[1] = ui.statusBit1Enable;
	statusBitEnable[2] = ui.statusBit2Enable;
	statusBitEnable[3] = ui.statusBit3Enable;
	statusBitEnable[4] = ui.statusBit4Enable;
	statusBitEnable[5] = ui.statusBit5Enable;
	statusBitEnable[6] = nullptr;
	statusBitEnable[7] = ui.statusBit7Enable;

	standardEventBit[0] = ui.stdEventBit0;
	standardEventBit[1] = ui.stdEventBit1;
	standardEventBit[2] = ui.stdEventBit2;
	standardEventBit[3] = ui.stdEventBit3;
	standardEventBit[4] = ui.stdEventBit4;
	standardEventBit[5] = ui.stdEventBit5;
	standardEventBit[6] = ui.stdEventBit6;
	standardEventBit[7] = ui.stdEventBit7;

	stdEventEnable[0] = ui.stdEvent0Enable;
	stdEventEnable[1] = ui.stdEvent1Enable;
	stdEventEnable[2] = ui.stdEvent2Enable;
	stdEventEnable[3] = ui.stdEvent3Enable;
	stdEventEnable[4] = ui.stdEvent4Enable;
	stdEventEnable[5] = ui.stdEvent5Enable;
	stdEventEnable[6] = ui.stdEvent6Enable;
	stdEventEnable[7] = ui.stdEvent7Enable;

	// init LED states and make enable checkbox connection
	for (int i = 0; i < 8; i++)
	{
		statusBit[i]->setLook(KLed::Flat);
		statusBit[i]->setColor(Qt::red);
		statusBit[i]->setState(KLed::Off);

		if (i != 6)
			connect(statusBitEnable[i], SIGNAL(stateChanged(int)), this, SLOT(statusByteEnableChanged(int)));

		standardEventBit[i]->setLook(KLed::Flat);
		standardEventBit[i]->setColor(Qt::red);
		standardEventBit[i]->setState(KLed::Off);

		connect(stdEventEnable[i], SIGNAL(stateChanged(int)), this, SLOT(stdEventEnableChanged(int)));
	}

	// std operation condition bits
	standardOperCondBit[0] = ui.stdOperCondBit0;
	standardOperCondBit[1] = ui.stdOperCondBit1;
	standardOperCondBit[2] = ui.stdOperCondBit2;
	standardOperCondBit[3] = ui.stdOperCondBit3;
	standardOperCondBit[4] = ui.stdOperCondBit4;
	standardOperCondBit[5] = ui.stdOperCondBit5;
	standardOperCondBit[6] = ui.stdOperCondBit6;
	standardOperCondBit[7] = ui.stdOperCondBit7;
	standardOperCondBit[8] = ui.stdOperCondBit8;
	standardOperCondBit[9] = ui.stdOperCondBit9;
	standardOperCondBit[10] = nullptr;
	standardOperCondBit[11] = ui.stdOperCondBit11;
	standardOperCondBit[12] = ui.stdOperCondBit12;
	standardOperCondBit[13] = ui.stdOperCondBit13;
	standardOperCondBit[14] = ui.stdOperCondBit14;
	standardOperCondBit[15] = ui.stdOperCondBit15;

	// init LED states
	for (int i = 0; i < 16; i++)
	{
		if (i != 10)
		{
			standardOperCondBit[i]->setLook(KLed::Flat);
			standardOperCondBit[i]->setColor(Qt::red);
			standardOperCondBit[i]->setState(KLed::Off);
		}
	}

	// std operation event bits
	standardOperEventBit[0] = nullptr;
	standardOperEventBit[1] = nullptr;
	standardOperEventBit[2] = ui.stdOperEventBit2;
	standardOperEventBit[3] = ui.stdOperEventBit3;
	standardOperEventBit[4] = ui.stdOperEventBit4;
	standardOperEventBit[5] = ui.stdOperEventBit5;
	standardOperEventBit[6] = nullptr;
	standardOperEventBit[7] = ui.stdOperEventBit7;
	standardOperEventBit[8] = ui.stdOperEventBit8;
	standardOperEventBit[9] = ui.stdOperEventBit9;
	standardOperEventBit[10] = ui.stdOperEventBit10;
	standardOperEventBit[11] = ui.stdOperEventBit11;
	standardOperEventBit[12] = ui.stdOperEventBit12;
	standardOperEventBit[13] = ui.stdOperEventBit13;
	standardOperEventBit[14] = ui.stdOperEventBit14;
	standardOperEventBit[15] = ui.stdOperEventBit15;

	stdOperEnable[0] = nullptr;
	stdOperEnable[1] = nullptr;
	stdOperEnable[2] = ui.stdOper2Enable;
	stdOperEnable[3] = ui.stdOper3Enable;
	stdOperEnable[4] = ui.stdOper4Enable;
	stdOperEnable[5] = ui.stdOper5Enable;
	stdOperEnable[6] = nullptr;
	stdOperEnable[7] = ui.stdOper7Enable;
	stdOperEnable[8] = ui.stdOper8Enable;
	stdOperEnable[9] = ui.stdOper9Enable;
	stdOperEnable[10] = ui.stdOper10Enable;
	stdOperEnable[11] = ui.stdOper11Enable;
	stdOperEnable[12] = ui.stdOper12Enable;
	stdOperEnable[13] = ui.stdOper13Enable;
	stdOperEnable[14] = ui.stdOper14Enable;
	stdOperEnable[15] = ui.stdOper15Enable;

	// init LED states and make enable bit connection
	for (int i = 0; i < 16; i++)
	{
		if (i > 1 && i != 6)
		{
			standardOperEventBit[i]->setLook(KLed::Flat);
			standardOperEventBit[i]->setColor(Qt::red);
			standardOperEventBit[i]->setState(KLed::Off);

			connect(stdOperEnable[i], SIGNAL(stateChanged(int)), this, SLOT(stdOperEnableChanged(int)));
		}
	}
	
	// make connections
	connect(ui.stdEventRegButton, SIGNAL(clicked()), this, SLOT(getStdEventReg()));
	connect(ui.stdOperationEventButton, SIGNAL(clicked()), this, SLOT(getStdOperationEventReg()));
}

//---------------------------------------------------------------------------
void magnetdaq::clearStatus(void)
{
	isSyncing.store(true);

	// if firmware does not have Std Operation register support, disable it
	if (supports_StdOperReg())
		ui.stdOperationGroupBox->setEnabled(true);
	else
		ui.stdOperationGroupBox->setEnabled(false);

	// clear all LEDs and enable value checkboxes and display
	for (int i = 0; i < 16; i++)
	{
		if (i < 8)
		{
			statusBit[i]->setState(KLed::Off);
			standardEventBit[i]->setState(KLed::Off);
			stdEventEnable[i]->setChecked(false);
		}

		if (i != 6 && i < 8)
			statusBitEnable[i]->setChecked(false);

		if (i != 10)
			standardOperCondBit[i]->setState(KLed::Off);

		if (i > 1 && i != 6)
		{
			standardOperEventBit[i]->setState(KLed::Off);
			stdOperEnable[i]->setChecked(false);
		}
	}

	ui.statusByteEnableEdit->clear();
	ui.stdEventEnableEdit->clear();
	ui.stdOperationEnableEdit->clear();

	isSyncing.store(false);
}

//---------------------------------------------------------------------------
void magnetdaq::refreshStatus(void)
{
	if (socket)
	{
		// get Status Byte
		socket->getStatusByte();

		for (int i = 0; i < 8; i++)
		{
			if (model430.statusByte() & (1 << i))
				statusBit[i]->setState(KLed::On);
			else
				statusBit[i]->setState(KLed::Off);
		}

		if (supports_StdOperReg())
		{
			// get Standard Operation Condition word
			socket->sendQuery("STAT:OPER:COND?\r\n", QueryState::STD_OPERATION_CONDITION);

			for (int i = 0; i < 16; i++)
			{
				if (i != 10)
				{
					if (model430.stdOperationCondition() & (1 << i))
						standardOperCondBit[i]->setState(KLed::On);
					else
						standardOperCondBit[i]->setState(KLed::Off);
				}
			}
		}
	}
}

//---------------------------------------------------------------------------
void magnetdaq::syncStatusSettings(void)
{
	isSyncing.store(true);

	if (socket)
	{
		socket->sendQuery("*SRE?\r\n", QueryState::STATUS_BYTE_ENABLE);
		socket->sendQuery("*ESE?\r\n", QueryState::STD_EVENT_ENABLE);
		if (supports_StdOperReg())
			socket->sendQuery("STAT:OPER:ENAB?\r\n", QueryState::STD_OPERATION_EVENT_ENABLE);

		// update Status Byte Enable
		ui.statusByteEnableEdit->setText(QString::number(model430.statusByteEnable()));

		for (int i = 0; i < 8; i++)
		{
			if (i != 6)
			{
				if (model430.statusByteEnable() & (1 << i))
					statusBitEnable[i]->setChecked(true);
				else
					statusBitEnable[i]->setChecked(false);
			}
		}

		// update Std Event Enable
		ui.stdEventEnableEdit->setText(QString::number(model430.stdEventEnable()));

		for (int i = 0; i < 8; i++)
		{
			if (model430.stdEventEnable() & (1 << i))
				stdEventEnable[i]->setChecked(true);
			else
				stdEventEnable[i]->setChecked(false);
		}

		if (supports_StdOperReg())
		{
			// update Std Operation Enable
			ui.stdOperationEnableEdit->setText(QString::number(model430.stdOperationEnable()));

			for (int i = 0; i < 16; i++)
			{
				if (i > 1 && i != 6)
				{
					if (model430.stdOperationEnable() & (1 << i))
						stdOperEnable[i]->setChecked(true);
					else
						stdOperEnable[i]->setChecked(false);
				}
			}
		}
	}

	isSyncing.store(false);
}

//---------------------------------------------------------------------------
void magnetdaq::getStdEventReg(void)
{
	if (socket)
	{
		// get Standard Event Register
		socket->sendQuery("*ESR?\r\n", QueryState::STD_EVENT_REGISTER);

		for (int i = 0; i < 8; i++)
		{
			if (model430.stdEventRegister() & (1 << i))
				standardEventBit[i]->setState(KLed::On);
			else
				standardEventBit[i]->setState(KLed::Off);
		}
	}
}

//---------------------------------------------------------------------------
void magnetdaq::getStdOperationEventReg(void)
{
	if (supports_StdOperReg())
	{
		if (socket)
		{
			// get Standard Operation Event Register
			socket->sendQuery("STAT:OPER?\r\n", QueryState::STD_OPERATION_EVENT_REGISTER);

			for (int i = 0; i < 16; i++)
			{
				if (i > 1 && i != 6)
				{
					if (model430.stdOperationEventRegister() & (1 << i))
						standardOperEventBit[i]->setState(KLed::On);
					else
						standardOperEventBit[i]->setState(KLed::Off);
				}
			}
		}
	}
}

//---------------------------------------------------------------------------
void magnetdaq::statusByteEnableChanged(int state)
{
	unsigned char temp = 0x00;

	if (!isSyncing.load())
	{
		for (int i = 0; i < 8; i++)
		{
			if (i != 6)
			{
				if (statusBitEnable[i]->isChecked())
					temp += (1 << i);
			}
		}

		if (socket)
		{
			socket->sendCommand("*SRE " + QString::number(temp) + "\r\n");
			model430.statusByteEnable = temp;
		}

		// update Status Byte Enable
		ui.statusByteEnableEdit->setText(QString::number(model430.statusByteEnable()));
	}
}

//---------------------------------------------------------------------------
void magnetdaq::stdEventEnableChanged(int state)
{
	unsigned char temp = 0x00;

	if (!isSyncing.load())
	{
		for (int i = 0; i < 8; i++)
		{
			if (stdEventEnable[i]->isChecked())
				temp += (1 << i);
		}

		if (socket)
		{
			socket->sendCommand("*ESE " + QString::number(temp) + "\r\n");
			model430.stdEventEnable = temp;
		}

		// update Std Event Enable
		ui.stdEventEnableEdit->setText(QString::number(model430.stdEventEnable()));
	}
}

//---------------------------------------------------------------------------
void magnetdaq::stdOperEnableChanged(int state)
{
	if (supports_StdOperReg())
	{
		unsigned short temp = 0x0000;

		if (!isSyncing.load())
		{
			for (int i = 0; i < 16; i++)
			{
				if (i > 1 && i != 6)
				{
					if (stdOperEnable[i]->isChecked())
						temp += (1 << i);
				}
			}

			if (socket)
			{
				socket->sendCommand("STAT:OPER:ENAB " + QString::number(temp) + "\r\n");
				model430.stdOperationEnable = temp;
			}

			// update Std Operation Enable
			ui.stdOperationEnableEdit->setText(QString::number(model430.stdOperationEnable()));
		}
	}
}

//---------------------------------------------------------------------------